﻿#include <iostream>
using namespace std;


void randomArray(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++) {
			if (arr[j] == arr[j + 1]) arr[j + 1] = rand() % 100;
			else arr[j] = rand() % 100;
		}
	}
}

void bubbleSort(int arr[], int n)
{
	int i, j;
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
				swap(arr[j], arr[j + 1]);
		}
	}
}

void printArray(int arr[], int size)
{
	int i;
	for (i = 0; i < size; i++)
	{
		cout << arr[i] << " ";
		cout << endl;
	}
}

void main()
{
	int arr[12] = {};
	int N = sizeof(arr) / sizeof(arr[0]);
	randomArray(arr, N);
	bubbleSort(arr, N);
	printArray(arr, N);
}